import { Trans } from '@lingui/macro'
import { useWeb3React } from '@web3-react/core'
import Web3Status from 'components/Web3Status'
import { NftListV2Variant, useNftListV2Flag } from 'featureFlags/flags/nftListV2'
import { chainIdToBackendName } from 'graphql/data/util'
import { useIsNftPage } from 'hooks/useIsNftPage'
import { Box } from 'nft/components/Box'
import { Row } from 'nft/components/Flex'
import { useProfilePageState } from 'nft/hooks'
import { ProfilePageStateType } from 'nft/types'
import { ReactNode } from 'react'
import { Link, NavLink, NavLinkProps, useLocation, useNavigate } from 'react-router-dom'
import styled from 'styled-components/macro'

import LogoIcon from '../../assets/images/logo.svg'
import { Bag } from './Bag'
import { ChainSelector } from './ChainSelector'
import { MenuDropdown } from './MenuDropdown'
import { SearchBar } from './SearchBar'
import * as styles from './style.css'

const Nav = styled.nav`
  padding: 20px 12px;
  width: 100%;
  margin-top: 20px;
  height: ${({ theme }) => theme.navHeight}px;
  z-index: 2;
`

interface MenuItemProps {
  href: string
  id?: NavLinkProps['id']
  isActive?: boolean
  children: ReactNode
  dataTestId?: string
}

const MenuItem = ({ href, dataTestId, id, isActive, children }: MenuItemProps) => {
  let swapActive = false;
  href == '/swap' ? swapActive = true : swapActive = false;
  if(href.startsWith('https://')){
    return (
      <a href={href} data-testid={dataTestId} id={id} className={isActive ? styles.activeMenuItem : styles.menuItem}  style={{ textDecoration: 'none', fontFamily: 'Basier Circle Medium, sans-serif'}}> {children}</a>
    )
  }
  return ( 
    <NavLink
      to={href}
      className={isActive ? styles.activeMenuItem : styles.menuItem}
      id={id}
      style={{ textDecoration: 'none', fontFamily: 'Basier Circle Medium, sans-serif'}}
      data-testid={dataTestId}
    >
      {children}
    </NavLink>
  )

}

export const PageTabs = () => {
  const { pathname } = useLocation()
  const { chainId: connectedChainId } = useWeb3React()
  const chainName = chainIdToBackendName(connectedChainId)

  const isPoolActive =
    pathname.startsWith('/pool') ||
    pathname.startsWith('/add') ||
    pathname.startsWith('/remove') ||
    pathname.startsWith('/increase') ||
    pathname.startsWith('/find')

  const isNftPage = useIsNftPage();
  return (
    <>
      <MenuItem href="https://pawecosystem.com">
        <Trans>Home</Trans>
      </MenuItem>
      <MenuItem href="/swap" isActive={pathname.startsWith('/swap')}>
        <Trans>Swap</Trans>
      </MenuItem>
      <MenuItem href="/pool" id="pool-nav-link" isActive={isPoolActive}>
        <Trans>Liquidity</Trans>
      </MenuItem>
      <MenuItem dataTestId="nft-nav" href="https://pawburn.io">
        <Trans>Burn</Trans>
      </MenuItem>
   
    </>
  )
}

const Navbar = () => {
  const isNftPage = useIsNftPage()
  const sellPageState = useProfilePageState((state) => state.state)
  const isNftListV2 = useNftListV2Flag() === NftListV2Variant.Enabled
  const navigate = useNavigate()

  return (
    <>
      <Nav>
        <Box display="flex" height="full" flexWrap="nowrap">
          <Box className={styles.leftSideContainer}>
            <Box className={styles.logoContainer} style={{ marginRight: '9%' }}>
              {/* <UniIcon
                width="48"
                height="48"
                data-testid="uniswap-logo"
                className={styles.logo}
                onClick={() => {
                  navigate({
                    pathname: '/',
                    search: '?intro=true',
                  })
                }}
              /> */}
              <img
                src={LogoIcon}
                alt="LogoIcon"
                onClick={() => window.location.href="https://pawecosystem.com"}
              />

            </Box>

            {!isNftPage && (
              <Box display={{ sm: 'flex', lg: 'none' }}>
                <ChainSelector leftAlign={true} />
              </Box>
            )}
            {/* <Box className={styles.searchContainer}>
              <SearchBar />
            </Box> */}

            <Row gap={{ xl: '0', xxl: '8' }} display={{ sm: 'none', lg: 'flex' }} style={{ backgroundColor: '#ffffff', borderRadius: 100, paddingRight: 25, paddingLeft: 25 }}>
              <PageTabs />
            </Row>
            <Box className={styles.rightSideContainer} style={{ width: 189 }}>
              <Row gap="0">
                {/* <Box position="relative" display={{ sm: 'flex', xl: 'none' }}>
                  <SearchBar />
                </Box> */}
                <Box display={{ sm: 'none', lg: 'flex' }}>
                  <MenuDropdown />
                </Box>
                {isNftPage && (!isNftListV2 || sellPageState !== ProfilePageStateType.LISTING) && <Bag />}

                <Web3Status />
              </Row>
            </Box>
          </Box>


        </Box>
      </Nav>
    </>
  )
}

export default Navbar
